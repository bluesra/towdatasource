package com.yyh.poc.twodatasource;

import java.util.List;

/**
 * Created by get67 on 5/1/2017.
 */
public interface InsertDao {
    public void doSimpleJdbcInsert(List<Object[]> works) throws Exception;
    public void doInPlainJdbc(List<Object[]> works) throws Exception;
    public void doJdbcTemplate(List<Object[]> works) throws Exception;
}
