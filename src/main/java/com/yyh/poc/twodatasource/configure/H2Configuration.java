package com.yyh.poc.twodatasource.configure;


import com.sun.org.apache.bcel.internal.generic.DASTORE;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Created by JACK YANG on 5/26/17.
 */
@Configuration
public class H2Configuration {
    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.h2")
    public DataSourceProperties h2DataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.h2")
    public DataSource h2DataSource() {
        return (HikariDataSource) h2DataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean
    @ConfigurationProperties("app.datasource.oracle")
    public DataSourceProperties oracleDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "oracleDS")
    @ConfigurationProperties("app.datasource.oracle")
    public DataSource oracleDataSource() {
        return oracleDataSourceProperties().initializeDataSourceBuilder().build();
    }


    @Bean(name = "oracleJT")
    public JdbcTemplate oracleJdbcTemplate(@Qualifier("oracleDS")DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
