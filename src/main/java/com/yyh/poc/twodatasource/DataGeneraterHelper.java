package com.yyh.poc.twodatasource;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

/**
 * Created by JACK YANG on 11/25/16.
 */
public class DataGeneraterHelper {


    public static List<Object[]> generateMockData(int size) {
        AtomicInteger index = new AtomicInteger(0);
        List<Object[]> list = new ArrayList<>();
        new Random().ints(size, -10, 10).forEach(offset -> {
            Object[] objects = new Object[3];
            objects[0] = index.incrementAndGet();
            objects[1] = String.valueOf(offset);
            objects[2] = "fixed value";
            list.add(objects);
        } );

//        list.forEach(item -> Arrays.asList(item).forEach(System.out::println));
        return list;
    }

    public static List<Map<String, Object>> generateMapMockData(List<Object[]> mockData) {
        return mockData.stream().map(item -> {
            Map<String, Object> mockMap = new HashMap<>();
            mockMap.put("ID", item[0]);
            mockMap.put("COLUMN1", item[1]);
            mockMap.put("COLUMN2", item[2]);
            return  mockMap;
        }).collect(Collectors.toList());

    }

}
