package com.yyh.poc.twodatasource;


import java.util.Date;

public class Customer {
    private long id;
    private String firstName, lastName;
    private Date happenTime;

    public Customer(long id, String firstName, String lastName, Date localDateTime) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.happenTime = localDateTime;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, firstName='%s', lastName='%s', happentime='%s']",
                id, firstName, lastName, happenTime.toString());
    }

    // getters & setters omitted for brevity
}

