package com.yyh.poc.twodatasource;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by get67 on 5/1/2017.
 */
@Component
public class InsertDaoImp implements InsertDao {
    private static final String insertSQL = "INSERT INTO PERFORMANCE_TEST(ID, COLUMN1, COLUMN2) VALUES (?,?,?)";

    @Autowired
    @Qualifier("oracleJT")
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Transactional
    public void doSimpleJdbcInsert(List<Object[]> works) throws Exception {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

        simpleJdbcInsert.withTableName("PERFORMANCE_TEST");
        String[] columns = {"ID","COLUMN1","COLUMN2"};
        simpleJdbcInsert.usingColumns(columns);
        List<Map<String, Object>> list = DataGeneraterHelper.generateMapMockData(works);
        for(List<Map<String, Object>> mapList : Lists.partition(list, 1000)) {
            simpleJdbcInsert.executeBatch(mapList.toArray(new Map[mapList.size()]));
        }
    }


    @Transactional
    public void doInPlainJdbc(List<Object[]> works) throws Exception {

        Connection dbConnection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
        PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);

        int[] idx = { 0 };

        works.forEach(obj -> {
            try {
                preparedStatement.setInt(1, (Integer) obj[0]);
                preparedStatement.setString(2, (String) obj[1]);
                preparedStatement.setString(3, (String) obj[2]);
                preparedStatement.addBatch();
                idx[0] ++;
                if(idx[0] % 1000 == 0) {
                    preparedStatement.executeBatch();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        preparedStatement.executeBatch();
    }

    public void doJdbcTemplate(List<Object[]> works) throws Exception {

        for(List<Object[]> eleList : Lists.partition(works, 1000)) {
            jdbcTemplate.batchUpdate(insertSQL, new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setInt(1, (Integer) eleList.get(i)[0]);
                    ps.setString(2, (String)  eleList.get(i)[1]);
                    ps.setString(3, (String)  eleList.get(i)[2]);
                }

                @Override
                public int getBatchSize() {
                    return eleList.size();
                }
            });

        }

    }
}
